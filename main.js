//[Section] Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

// There are two types of comments:
// -Single line comment (crtl /) denotade by two slashes(//)
/*
	-Multiline comment
	-denoted by / ** /
	-crtl + shift + /
*/

//[Section] Statements and Syntax
//	Statements
//	-In programming language, statements are instructions that we tell the computer to perform.
//	-JS statements usually ends with semicolon(;)
//		-it is also called delimeter
//	-Semicolons are not required in JS.
//		-it use to help us train or locate where a statement ends
//
//	Syntax
//	-In programming, it is the set of rules that describes how statements must be constructed.
//	Declaring a variable
// tells our devices that a variable name is created and is ready to store data
// 	Syntax : let/const variableName;
// let is a keyword that is usually used in declaring a variable.



let myVariable;
myVariable = "Hello";
console.log(myVariable);

//	Intitalize a value
		// storing the initial value of a varribale.
		// assignment operator


// console.log(myVariable);

// const PI;
// PI = 3.1416;

// console.log(PI);

let person = {
    fullname:"Juan Dela Cruz",
    age: 35,
    isMarried:false,
    contact:["09123456789","712 3546"],
    address:{
        houseNumber: "234",
        city: "Manila"
    }
};
console.log(person);

let myGrades= {
    firstGrading : 98.7,
    secondGrading : 92.1,
    thirdGrading : 90.2,
    fourthGrading : 94.6
}
console.log(myGrades);
console.log(typeof myGrades);
let spouse = null;
    console.log(spouse);
let fullName;
console.log(fullName);